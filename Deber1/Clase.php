<?php
 
Class CalAyP{
    public static function RectanguloArea($Largo, $Ancho) {
        $AreaR = $Ancho * $Largo;
        return $AreaR;
    }

    public static function RectanguloPerimetro($Largo, $Ancho) {
        $PerimetroR = ($Ancho * $Largo) * 2;
        return $PerimetroR;
    }

    public static function CuadradoArea($Largo) {
        $AreaC = $Largo * $Largo;
        return $AreaC;
    }

    public static function CuadradoPerimetro($Largo) {
        $PerimetroC = ($Largo) * 4;
        return $PerimetroC;
    }

    public static function trianguloArea($Largo, $Ancho) {
        $AreaT = ($Largo * $Ancho) / 2;
        return $AreaT;
    }

    public static function TrianguloPerimetro($Largo, $Ancho) {
        $PerimetroT = ($Ancho * 2) + $Largo;
        return $PerimetroT;
    }
 }
